﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ploomes.Infrastructure.Migrations
{
    public partial class AdicaoDataAtualizacaoNoTipoVeiculo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DataAtualizacao",
                table: "TiposVeiculos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataAtualizacao",
                table: "TiposVeiculos");
        }
    }
}
