﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ploomes.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ploomes.Infrastructure.Configuration
{
    class VeiculoConfiguration : IEntityTypeConfiguration<Veiculo>
    {
        public void Configure(EntityTypeBuilder<Veiculo> builder)
        {

            builder.HasKey(c => c.Id);

            builder
                .HasOne(s => s.MarcaVeiculo)
                .WithMany(u => u.Veiculos)
                .HasForeignKey(s => s.MarcaVeiculoId);

            var element = builder.Metadata.FindNavigation(nameof(Veiculo.ModelosVeiculos));
            element.SetField("_modelosVeiculos");
            element.SetPropertyAccessMode(PropertyAccessMode.Field);

        }
    }
}
