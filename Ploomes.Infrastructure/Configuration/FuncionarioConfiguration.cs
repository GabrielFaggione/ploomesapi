﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ploomes.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ploomes.Infrastructure.Configuration
{
    class FuncionarioConfiguration : IEntityTypeConfiguration<Funcionario>
    {
        public void Configure(EntityTypeBuilder<Funcionario> builder)
        {
            builder.HasKey(c => c.Id);

            var element = builder.Metadata.FindNavigation(nameof(Funcionario.TransacoesFinanceiras));
            element.SetField("_transacoesFinanceiras");
            element.SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
