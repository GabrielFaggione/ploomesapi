﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ploomes.Domain
{
    public class MarcaVeiculo
    {

        public int Id { get; private set; }
        public int IdFipe { get; private set; }
        public string Key { get; private set; }
        public string Nome { get; private set; }
        public string NomeFipe { get; private set; }

        // 1 (TipoVeiculo) - to - many (Marca)
        public TipoVeiculo TipoVeiculo { get; private set; }
        public int TipoVeiculoId { get; private set; }

        // 1 (Marca) - to - many (Veiculo)
        private HashSet<Veiculo> _veiculos;
        public IEnumerable<Veiculo> Veiculos => _veiculos?.ToList();

        private MarcaVeiculo()
        {
            _veiculos = new HashSet<Veiculo>();
        }

        public MarcaVeiculo(MarcaVeiculoApiDTO marcaVeiculo, TipoVeiculo tipoVeiculo)
        {
            IdFipe = marcaVeiculo.id;
            Key = marcaVeiculo.key;
            Nome = marcaVeiculo.name;
            NomeFipe = marcaVeiculo.fipe_name;

            TipoVeiculo = tipoVeiculo;
        }

        public void AdicionarVeiculo(VeiculoApiDTO veiculo)
        {
            var Veiculo = new Veiculo(veiculo);
            _veiculos.Add(Veiculo);
        }

    }

    #region DTOs
    public class MarcaVeiculoApiDTO
    {
        public string key { get; set; }
        public int id { get; set; }
        public string fipe_name { get; set; }
        public string name { get; set; }
    }

    public class MarcaVeiculoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public List<VeiculoDTO> Veiculos { get; set; }

        public MarcaVeiculoDTO(MarcaVeiculo marcaVeiculo)
        {
            Veiculos = new List<VeiculoDTO>();
            Id = marcaVeiculo.Id;
            Nome = marcaVeiculo.Nome;
            foreach (var veiculo in marcaVeiculo.Veiculos)
            {
                Veiculos.Add(new VeiculoDTO(veiculo));
            }
        }
    }

    public class MarcaVeiculoSimplesDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public MarcaVeiculoSimplesDTO(MarcaVeiculo marcaVeiculo)
        {
            Id = marcaVeiculo.Id;
            Nome = marcaVeiculo.Nome;
        }
    }
    #endregion

    #region Interface
    public interface IMarcaVeiculoService
    {
        public Task AtualizarVeiculosAsync(MarcaVeiculo marcaVeiculo);
        public Task<List<MarcaVeiculoSimplesDTO>> GetMarcasVeiculosPorTipoVeiculoId(int id);
    }
    #endregion
}
