﻿using Microsoft.EntityFrameworkCore;
using Ploomes.Domain;
using Ploomes.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ploomes.API.Services
{
    public class VeiculoService : IVeiculoService
    {

        public VeiculoService(
            PloomesContext context
            )
        {
            Context = context;
        }

        public PloomesContext Context;

        public async Task<List<VeiculoDTO>> GetVeiculosPorMarcaVeiculoId(int id)
        {

            var marcaVeiculo = await Context.MarcasVeiculos
                .Include(i => i.Veiculos)
                .FirstOrDefaultAsync(f => f.Id == id);
            var veiculosDTO = marcaVeiculo.Veiculos.Select(s => new VeiculoDTO(s)).ToList();

            return veiculosDTO;

        }
    }
}
