﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ploomes.Domain
{
    public class ModeloVeiculo
    {

        public int Id { get; private set; }
        public string IdFipe { get; private set; }
        public string Key { get; private set; }
        public string CodigoFipe { get; private set; }
        public string Nome { get; private set; }
        public string Referencia { get; private set; }
        public string Combustivel { get; private set; }
        public double Preco { get; private set; }
        // 1 (Veiculo) - to - many (Modelo)
        public Veiculo Veiculo { get; private set; }
        public int VeiculoId { get; private set; }
        // 1 (Modelo) - to - many (TransacaoFinanceira)
        private HashSet<TransacaoFinanceira> _transacoesFinanceiras;
        public IEnumerable<TransacaoFinanceira> TransacoesFinanceiras => _transacoesFinanceiras?.ToList();
    
        private ModeloVeiculo()
        {
        }

        public ModeloVeiculo(ModeloVeiculoApiDTO modeloDTO, Veiculo veiculo)
        {
            IdFipe = modeloDTO.id;
            Nome = modeloDTO.name;
            Key = modeloDTO.key;
            CodigoFipe = modeloDTO.fipe_codigo;

            Veiculo = veiculo;
        }

        public void AtualizarModelo(ModeloVeiculoCompletoApiDTO modeloDTO)
        {
            var preco = modeloDTO.preco
                .Replace("R$", "")
                .Replace(".", "")
                .Trim();

            Preco = Double.Parse(preco);
            Combustivel = modeloDTO.combustivel;
            Referencia = modeloDTO.referencia;
        }

    }

    #region DTOs
    public class ModeloVeiculoApiDTO
    {
        public string fipe_marca { get; set; }
        public string fipe_codigo { get; set; }
        public string name { get; set; }
        public string key { get; set; }
        public string veiculo { get; set; }
        public string id { get; set; }
        public string marca { get; set; }
    }

    public class ModeloVeiculoCompletoApiDTO
    {
        public string id { get; set; }
        public string ano_modelo { get; set; }
        public string marca { get; set; }
        public string name { get; set; }
        public string veiculo { get; set; }
        public string preco { get; set; }
        public string combustivel { get; set; }
        public string referencia { get; set; }
        public string fipe_codigo { get; set; }
        public string key { get; set; }
    }

    public class ModeloVeiculoDTO
    {
        public int Id { get; private set; }
        public string CodigoFipe { get; private set; }
        public string Nome { get; private set; }
        public string Referencia { get; private set; }
        public string Combustivel { get; private set; }
        public double Preco { get; private set; }

        public ModeloVeiculoDTO(ModeloVeiculo modelo)
        {
            Id = modelo.Id;
            CodigoFipe = modelo.CodigoFipe;
            Nome = modelo.Nome;
            Referencia = modelo.Referencia;
            Combustivel = modelo.Combustivel;
            Preco = modelo.Preco;
        }
    }
    #endregion

    #region Interface
    public interface IModeloVeiculoService
    {
        public Task<List<ModeloVeiculoDTO>> GetModelosVeiculosPorVeiculoId(int id);
        public Task AtualizarModelosAsync(Veiculo veiculo);
        public Task AtualizarInformacoesModeloAsync(ModeloVeiculo modelo, Veiculo veiculo);
    }
    #endregion
}
