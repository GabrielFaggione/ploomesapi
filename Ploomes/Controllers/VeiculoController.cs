﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ploomes.API.Services;
using Ploomes.Domain;

namespace Ploomes.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VeiculoController : ControllerBase
    {
        public VeiculoController(
            ITipoVeiculoService tipoVeiculoService,
            IMarcaVeiculoService marcaVeiculoService,
            IVeiculoService veiculoService,
            IModeloVeiculoService modeloVeiculoService
            )
        {
            TipoVeiculoService = tipoVeiculoService;
            MarcaVeiculoService = marcaVeiculoService;
            VeiculoService = veiculoService;
            ModeloVeiculoService = modeloVeiculoService;
        }

        ITipoVeiculoService TipoVeiculoService;
        IMarcaVeiculoService MarcaVeiculoService;
        IVeiculoService VeiculoService;
        IModeloVeiculoService ModeloVeiculoService;

        [HttpGet("[action]")]
        public async Task<IActionResult> GetTodosTiposVeiculos()
        {
            try
            {
                var tiposVeiculos = await TipoVeiculoService.GetTodosTiposVeiculos();
                return Ok(tiposVeiculos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetMarcasVeiculosPorTipoVeiculo(int tipoVeiculoId)
        {
            try
            {
                var marcasVeiculos = await MarcaVeiculoService.GetMarcasVeiculosPorTipoVeiculoId(tipoVeiculoId);
                return Ok(marcasVeiculos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetVeiculosPorMarcaVeiculoId(int marcaVeiculoId)
        {
            try
            {
                var veiculos = await VeiculoService.GetVeiculosPorMarcaVeiculoId(marcaVeiculoId);
                return Ok(veiculos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetModelosPorVeiculoId(int veiculoId)
        {
            try
            {
                var modelos = await ModeloVeiculoService.GetModelosVeiculosPorVeiculoId(veiculoId);
                return Ok(modelos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


    }
}
