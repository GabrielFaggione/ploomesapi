﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ploomes.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ploomes.Infrastructure.Configuration
{
    class ModeloVeiculoConfiguration : IEntityTypeConfiguration<ModeloVeiculo>
    {
        public void Configure(EntityTypeBuilder<ModeloVeiculo> builder)
        {

            builder.HasKey(c => c.Id);

            builder
                .HasOne(s => s.Veiculo)
                .WithMany(u => u.ModelosVeiculos)
                .HasForeignKey(s => s.VeiculoId);

            var element = builder.Metadata.FindNavigation(nameof(ModeloVeiculo.TransacoesFinanceiras));
            element.SetField("_transacoesFinanceiras");
            element.SetPropertyAccessMode(PropertyAccessMode.Field);

        }
    }
}
