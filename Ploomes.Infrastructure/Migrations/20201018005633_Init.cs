﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ploomes.Infrastructure.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TiposVeiculos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(nullable: true),
                    NomeFipe = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposVeiculos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MarcasVeiculos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdFipe = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Nome = table.Column<string>(nullable: true),
                    NomeFipe = table.Column<string>(nullable: true),
                    TipoVeiculoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarcasVeiculos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarcasVeiculos_TiposVeiculos_TipoVeiculoId",
                        column: x => x.TipoVeiculoId,
                        principalTable: "TiposVeiculos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Veiculos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdFipe = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Nome = table.Column<string>(nullable: true),
                    NomeFipe = table.Column<string>(nullable: true),
                    MarcaVeiculoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Veiculos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Veiculos_MarcasVeiculos_MarcaVeiculoId",
                        column: x => x.MarcaVeiculoId,
                        principalTable: "MarcasVeiculos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ModelosVeiculos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdFipe = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    CodigoFipe = table.Column<string>(nullable: true),
                    Nome = table.Column<string>(nullable: true),
                    Referencia = table.Column<string>(nullable: true),
                    Combustivel = table.Column<string>(nullable: true),
                    Preco = table.Column<double>(nullable: false),
                    VeiculoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModelosVeiculos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ModelosVeiculos_Veiculos_VeiculoId",
                        column: x => x.VeiculoId,
                        principalTable: "Veiculos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MarcasVeiculos_TipoVeiculoId",
                table: "MarcasVeiculos",
                column: "TipoVeiculoId");

            migrationBuilder.CreateIndex(
                name: "IX_ModelosVeiculos_VeiculoId",
                table: "ModelosVeiculos",
                column: "VeiculoId");

            migrationBuilder.CreateIndex(
                name: "IX_Veiculos_MarcaVeiculoId",
                table: "Veiculos",
                column: "MarcaVeiculoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ModelosVeiculos");

            migrationBuilder.DropTable(
                name: "Veiculos");

            migrationBuilder.DropTable(
                name: "MarcasVeiculos");

            migrationBuilder.DropTable(
                name: "TiposVeiculos");
        }
    }
}
