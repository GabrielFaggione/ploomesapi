﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ploomes.Domain
{
    public class Veiculo
    {

        public int Id { get; private set; }
        public int IdFipe { get; private set; }
        public string Key { get; private set; }
        public string Nome { get; private set; }
        public string NomeFipe { get; private set; }

        // 1 (MarcaVeiculo) - to - many (Veiculo)
        public MarcaVeiculo MarcaVeiculo { get; private set; }
        public int MarcaVeiculoId { get; private set; }

        // 1 (ModeloVeiculo) - to - many (Veiculo)
        private HashSet<ModeloVeiculo> _modelosVeiculos;
        public IEnumerable<ModeloVeiculo> ModelosVeiculos => _modelosVeiculos?.ToList();

        private Veiculo()
        {
            _modelosVeiculos = new HashSet<ModeloVeiculo>();
        }

        public Veiculo(VeiculoApiDTO veiculo)
        {
            IdFipe = veiculo.id;
            Nome = veiculo.name;
            NomeFipe = veiculo.fipe_name;
            Key = veiculo.key;
        }

        public void AdicionarModelo(ModeloVeiculoApiDTO modeloDTO)
        {
            var Modelo = new ModeloVeiculo(modeloDTO, this);
            _modelosVeiculos.Add(Modelo);
        }

    }

    #region DTOs
    public class VeiculoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public VeiculoDTO(Veiculo veiculo)
        {
            Id = veiculo.Id;
            Nome = veiculo.Nome;
        }

    }

    public class VeiculoApiDTO
    {
        public string key { get; set; }
        public string name { get; set; }
        public int id { get; set; }
        public string fipe_name { get; set; }
    }
    #endregion

    #region Interface
    public interface IVeiculoService
    {
        public Task<List<VeiculoDTO>> GetVeiculosPorMarcaVeiculoId(int id);
    }
    #endregion
}
