using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Ploomes.Infrastructure;
using Microsoft.OpenApi.Models;
using Ploomes.API.Services;
using Ploomes.Domain;

namespace Ploomes
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ITipoVeiculoService, TipoVeiculoService>();
            services.AddScoped<IMarcaVeiculoService, MarcaVeiculoService>();
            services.AddScoped<IVeiculoService, VeiculoService>();
            services.AddScoped<IModeloVeiculoService, ModeloVeiculoService>();
            services.AddScoped<IFuncionarioService, FuncionarioService>();
            services.AddScoped<ITransacaoFinanceiraService, TransacaoFinanceiraService>();

            services.AddControllers().AddNewtonsoftJson(options => 
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

#if (DEBUG)
            services.AddDbContext<PloomesContext>(options => 
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
#else
            services.AddDbContext<PloomesContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("ReleaseConnection")));
#endif

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "Ploomes API",
                        Version = "v1",
                        Description = "API de consultas em tempo real da tabela FIPE"
                    });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PloomesAPI V1");
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
