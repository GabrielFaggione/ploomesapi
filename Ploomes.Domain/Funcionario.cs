﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ploomes.Domain
{
    public class Funcionario
    {
        public int Id { get; private set; }
        public bool Ativo { get; private set; }
        public string Nome { get; private set; }
        public string Documento { get; private set; }
        // 1 (Funcionario) - to - many (TransacaoFinanceira)
        private HashSet<TransacaoFinanceira> _transacoesFinanceiras;
        public IEnumerable<TransacaoFinanceira> TransacoesFinanceiras => _transacoesFinanceiras?.ToList(); 

        private Funcionario()
        {
            _transacoesFinanceiras = new HashSet<TransacaoFinanceira>();
        }

        public Funcionario(FuncionarioSimplesDTO funcionario)
        {
            Nome = funcionario.Nome;
            Documento = funcionario.Documento;
            Ativo = true;
        }

        public void Desativar()
        {
            Ativo = false;  
        }
    }

    #region DTOs
    public class FuncionarioSimplesDTO
    {
        public bool Ativo { get; set; }
        public string Nome { get; set; }
        public string Documento { get; set; }
        
        public FuncionarioSimplesDTO() { }

        public FuncionarioSimplesDTO(Funcionario funcionario)
        {
            Ativo = funcionario.Ativo;
            Nome = funcionario.Nome;
            Documento = funcionario.Documento;
        }
    }

    public class FuncionarioDTO
    {
        public string Nome { get; set; }
        public string Documento { get; set; }
        public List<TransacaoFinanceiraDTO> TransacoesFinanceiras { get; set; }
    }
    #endregion

    #region Interface
    public interface IFuncionarioService
    {
        public Task<List<FuncionarioSimplesDTO>> ListarTodosFuncionariosAsync();
        public Task<List<TransacaoFinanceiraCompletaDTO>> ListarTodasTransacoesPorFuncionarioId(int funcionarioId);
        public Task CadastrarNovoFuncionario(FuncionarioSimplesDTO funcionario);
        public Task DesativarFuncionario(int funcionarioId);
        public List<TransacaoFinanceiraDTO> RecuperarTransacoesFinanceirasPorUsuarioId(int funcionarioId);
    }
    #endregion
}
