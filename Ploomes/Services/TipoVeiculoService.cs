﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Ploomes.Domain;
using Ploomes.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ploomes.API.Services
{
    public class TipoVeiculoService : ITipoVeiculoService
    {

        public TipoVeiculoService(
            PloomesContext context
            )
        {
            Context = context;
        }

        private readonly PloomesContext Context;

        public async Task AtualizarMarcasVeiculosAsync(TipoVeiculo tipoVeiculo)
        {
            using (var tran = await Context.Database.BeginTransactionAsync())
            {
                if (tipoVeiculo.DataAtualizacao.HasValue)
                {
                    if ((DateTime.Now - tipoVeiculo.DataAtualizacao.Value).TotalDays > 1)
                    {
                        using (var cliente = new HttpClient())
                        {
                            var resposta = await cliente.GetAsync(string.Format("http://fipeapi.appspot.com/api/1/{0}/marcas.json", tipoVeiculo.NomeFipe));
                            resposta.EnsureSuccessStatusCode();
                            var respotaBody = await resposta.Content.ReadAsStringAsync();
                            var respostaDeserializada = JsonConvert.DeserializeObject<List<MarcaVeiculoApiDTO>>(respotaBody);

                            foreach (var marcaVeiculoDTO in respostaDeserializada)
                            {
                                if (!tipoVeiculo.MarcasVeiculos.Any(a => a.IdFipe == marcaVeiculoDTO.id))
                                {
                                    tipoVeiculo.AdicionarMarcaVeiculo(marcaVeiculoDTO);
                                }
                            }
                        }
                        tipoVeiculo.AtualizarDataAtualizacao();
                    }
                    await Context.SaveChangesAsync();
                    await tran.CommitAsync();
                }
            }
        }

        public async Task<List<TipoVeiculoDTO>> GetTodosTiposVeiculos()
        {
            var tiposVeiculos = await Context.TiposVeiculos.ToListAsync();
            var tiposVeiculosDTO = new List<TipoVeiculoDTO>();
            foreach (var tipoVeiculo in tiposVeiculos)
            {
                await AtualizarMarcasVeiculosAsync(tipoVeiculo);
                tiposVeiculosDTO.Add(new TipoVeiculoDTO { Id = tipoVeiculo.Id, Nome = tipoVeiculo.Nome });
            }
            return tiposVeiculosDTO;
        }
    }
}
