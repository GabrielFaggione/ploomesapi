﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Ploomes.Domain;
using Ploomes.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ploomes.API.Services
{
    public class ModeloVeiculoService : IModeloVeiculoService
    {

        public ModeloVeiculoService(
            PloomesContext context
            )
        {
            Context = context;
        }

        public PloomesContext Context;


        public async Task AtualizarInformacoesModeloAsync(ModeloVeiculo modelo, Veiculo veiculo)
        {
            if (modelo.Preco == 0)
            {
                using (var tran = await Context.Database.BeginTransactionAsync())
                {
                    using (var cliente = new HttpClient())
                    {
                        var resposta = await cliente.GetAsync(string.Format("http://fipeapi.appspot.com/api/1/{0}/veiculo/{1}/{2}/{3}.json",
                                veiculo.MarcaVeiculo.TipoVeiculo.NomeFipe, veiculo.MarcaVeiculo.IdFipe, veiculo.IdFipe, modelo.IdFipe));
                        resposta.EnsureSuccessStatusCode();
                        var respotaBody = await resposta.Content.ReadAsStringAsync();
                        var respostaDeserializada = JsonConvert.DeserializeObject<ModeloVeiculoCompletoApiDTO>(respotaBody);

                        modelo.AtualizarModelo(respostaDeserializada);
                    }
                    await Context.SaveChangesAsync();
                    await tran.CommitAsync();
                }
            }
        }

        public async Task AtualizarModelosAsync(Veiculo veiculo)
        {
            if (veiculo.ModelosVeiculos.Count() == 0)
            {
                using (var tran = await Context.Database.BeginTransactionAsync())
                {
                    using (var cliente = new HttpClient())
                    {
                        var resposta = await cliente.GetAsync(string.Format("http://fipeapi.appspot.com/api/1/{0}/veiculo/{1}/{2}.json",
                                veiculo.MarcaVeiculo.TipoVeiculo.NomeFipe, veiculo.MarcaVeiculo.IdFipe, veiculo.IdFipe));
                        resposta.EnsureSuccessStatusCode();
                        var respotaBody = await resposta.Content.ReadAsStringAsync();
                        var respostaDeserializada = JsonConvert.DeserializeObject<List<ModeloVeiculoApiDTO>>(respotaBody);

                        foreach (var modeloVeiculo in respostaDeserializada)
                        {
                            if (!veiculo.ModelosVeiculos.Any(a => a.IdFipe == modeloVeiculo.id))
                            {
                                veiculo.AdicionarModelo(modeloVeiculo);
                            }
                        }
                    }
                    await Context.SaveChangesAsync();
                    await tran.CommitAsync();
                }
            }
        }

        public async Task<List<ModeloVeiculoDTO>> GetModelosVeiculosPorVeiculoId(int id)
        {
            var veiculo = await Context.Veiculos
                .Include(v => v.ModelosVeiculos)
                .Include(v => v.MarcaVeiculo)
                    .ThenInclude(v => v.TipoVeiculo)
                .FirstOrDefaultAsync(f => f.Id == id);
            await AtualizarModelosAsync(veiculo);
            foreach (var modelo in veiculo.ModelosVeiculos)
            {
                await AtualizarInformacoesModeloAsync(modelo, veiculo);
            }
            var modelos = veiculo.ModelosVeiculos
                .Select(s => new ModeloVeiculoDTO(s))
                .ToList();

            return modelos;
        }
    }
}
