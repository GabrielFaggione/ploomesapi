﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ploomes.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ploomes.Infrastructure.Configuration
{
    class TipoVeiculoConfiguration : IEntityTypeConfiguration<TipoVeiculo>
    {
        public void Configure(EntityTypeBuilder<TipoVeiculo> builder)
        {
            builder.HasKey(c => c.Id);

            var element = builder.Metadata.FindNavigation(nameof(TipoVeiculo.MarcasVeiculos));
            element.SetField("_marcasVeiculos");
            element.SetPropertyAccessMode(PropertyAccessMode.Field);
        }

    }
}
