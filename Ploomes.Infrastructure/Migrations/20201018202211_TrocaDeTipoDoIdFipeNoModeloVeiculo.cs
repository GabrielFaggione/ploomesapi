﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ploomes.Infrastructure.Migrations
{
    public partial class TrocaDeTipoDoIdFipeNoModeloVeiculo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IdFipe",
                table: "ModelosVeiculos",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IdFipe",
                table: "ModelosVeiculos",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
