﻿using Microsoft.EntityFrameworkCore;
using Ploomes.Domain;
using Ploomes.Infrastructure.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ploomes.Infrastructure
{
    public class PloomesContext : DbContext
    {

        public PloomesContext(DbContextOptions<PloomesContext> options)
            : base(options)
        { }

        // Espaço para os DbSet
        public DbSet<TipoVeiculo> TiposVeiculos { get; set; }
        public DbSet<MarcaVeiculo> MarcasVeiculos { get; set; }
        public DbSet<Veiculo> Veiculos { get; set; }
        public DbSet<ModeloVeiculo> ModelosVeiculos { get; set; }
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<TransacaoFinanceira> TransacoesFinanceiras { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TipoVeiculoConfiguration());
            modelBuilder.ApplyConfiguration(new MarcaVeiculoConfiguration());
            modelBuilder.ApplyConfiguration(new VeiculoConfiguration());
            modelBuilder.ApplyConfiguration(new ModeloVeiculoConfiguration());
            modelBuilder.ApplyConfiguration(new FuncionarioConfiguration());
            modelBuilder.ApplyConfiguration(new TransacaoFinanceiraConfiguration());
        }

    }
}
