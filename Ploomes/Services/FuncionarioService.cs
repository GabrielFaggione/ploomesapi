﻿using Microsoft.EntityFrameworkCore;
using Ploomes.Domain;
using Ploomes.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ploomes.API.Services
{
    public class FuncionarioService : IFuncionarioService
    {

        public FuncionarioService(
            PloomesContext context
            )
        {
            Context = context;
        }

        private readonly PloomesContext Context;

        public async Task CadastrarNovoFuncionario(FuncionarioSimplesDTO funcionario)
        {
            using (var tran = Context.Database.BeginTransaction())
            {
                var Funcionario = new Funcionario(funcionario);
                Context.Add(Funcionario);
                await Context.SaveChangesAsync();
                await tran.CommitAsync();
            }
        }

        public async Task DesativarFuncionario(int funcionarioId)
        {
            using (var tran = Context.Database.BeginTransaction())
            {
                var funcionario = Context.Funcionarios.FirstOrDefault(f => f.Id == funcionarioId);
                funcionario.Desativar();
                await Context.SaveChangesAsync();
                await tran.CommitAsync();
            }
        }

        public List<TransacaoFinanceiraDTO> RecuperarTransacoesFinanceirasPorUsuarioId(int funcionarioId)
        {
            var funcionario = Context.Funcionarios
                .Include(i => i.TransacoesFinanceiras)
                    .ThenInclude(i => i.ModeloVeiculo)
                .FirstOrDefault(f => f.Id == funcionarioId);
            var transacoes = funcionario.TransacoesFinanceiras
                .Select(s => new TransacaoFinanceiraDTO(s))
                .ToList();

            return transacoes;
        }

        public async Task<List<FuncionarioSimplesDTO>> ListarTodosFuncionariosAsync()
        {
            var funcionarios = await Context.Funcionarios
                .Select(s => new FuncionarioSimplesDTO(s))
                .ToListAsync();
            return funcionarios;
        }

        public async Task<List<TransacaoFinanceiraCompletaDTO>> ListarTodasTransacoesPorFuncionarioId(int funcionarioId)
        {
            var funcionario = await Context.Funcionarios
                .Include(i => i.TransacoesFinanceiras)
                    .ThenInclude(i => i.ModeloVeiculo)
                    .ThenInclude(i => i.Veiculo)
                    .ThenInclude(i => i.MarcaVeiculo)
                    .ThenInclude(i => i.TipoVeiculo)
                .FirstOrDefaultAsync(f => f.Id == funcionarioId);
            var transacoes = funcionario.TransacoesFinanceiras
                .Select(s => new TransacaoFinanceiraCompletaDTO(s))
                .ToList();

            return transacoes;
        }
    }
}
