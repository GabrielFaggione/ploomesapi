﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ploomes.Domain;
using Ploomes.Infrastructure;

namespace Ploomes.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FuncionarioController : ControllerBase
    {

        public FuncionarioController(
            IFuncionarioService funcionarioService
            )
        {
            FuncionarioService = funcionarioService;
        }

        private readonly IFuncionarioService FuncionarioService;

        [HttpGet("[action]")]
        public async Task<IActionResult> GetTodosFuncionarios()
        {
            try
            {
                var funcionarios = await FuncionarioService.ListarTodosFuncionariosAsync();
                return Ok(funcionarios);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetTodasTransacoesFinanceirasPorFuncionarioId(int funcionarioId)
        {
            try
            {
                var transacoes = await FuncionarioService.ListarTodasTransacoesPorFuncionarioId(funcionarioId);
                return Ok(transacoes);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> PostAdicionarNovoFuncionario(FuncionarioSimplesDTO funcionarioDTO)
        {
            try
            {
                await FuncionarioService.CadastrarNovoFuncionario(funcionarioDTO);
                return Ok("Funcionário Cadastrado");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> PutDesativarFuncionario(int funcionarioId)
        {
            try
            {
                await FuncionarioService.DesativarFuncionario(funcionarioId);
                return Ok("Funcionário Desativado");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}
