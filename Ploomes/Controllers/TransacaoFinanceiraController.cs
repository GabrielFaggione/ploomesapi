﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ploomes.Domain;

namespace Ploomes.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransacaoFinanceiraController : ControllerBase
    {
        public TransacaoFinanceiraController(
            ITransacaoFinanceiraService transacaoFinanceiraService
        )
        {
            TransacaoFinanceiraService = transacaoFinanceiraService;
        }

        private readonly ITransacaoFinanceiraService TransacaoFinanceiraService;

        [HttpPost("[action]")]
        public async Task<IActionResult> CriarNovaTransacaoFinanceira(TransacaoFinanceiraDTO transacaoFinanceira)
        {
            try
            {
                await TransacaoFinanceiraService.CriarNovaTransacaoFinanceira(transacaoFinanceira);
                return Ok("Transação Cadastrada");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}
