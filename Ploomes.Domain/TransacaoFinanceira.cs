﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ploomes.Domain
{
    public class TransacaoFinanceira
    {

        public int Id { get; private set; }
        public string Tipo { get; private set; }
        public double Valor { get; private set; }
        public DateTime Data { get; private set; }

        // 1 (ModeloVeiculo) - to - many (TransacaoFinanceira) 
        public ModeloVeiculo ModeloVeiculo { get; private set; }
        public int ModeloVeiculoId { get; private set; }
        // 1 (Funcionario) - to - many (TransacaoFinanceira) 
        public Funcionario Funcionario { get; private set; }
        public int FuncionarioId { get; private set; }

        private TransacaoFinanceira()
        {
        }

        public TransacaoFinanceira(TransacaoFinanceiraDTO transacaoDTO)
        {
            Tipo = transacaoDTO.Tipo;
            Valor = transacaoDTO.Valor;
            Data = DateTime.Now;

            ModeloVeiculoId = transacaoDTO.ModeloId;

            FuncionarioId = transacaoDTO.FuncionarioId;
        }

    }

    #region DTOs
    public class TransacaoFinanceiraDTO
    {
        public string Tipo { get; set; }
        public double Valor { get; set; }
        public DateTime Data { get; set; }
        public ModeloVeiculoDTO Modelo { get; set; }
        public int ModeloId { get; set; }
        public FuncionarioSimplesDTO Funcionario { get; set; }
        public int FuncionarioId { get; set; }

        public TransacaoFinanceiraDTO() { }

        public TransacaoFinanceiraDTO(TransacaoFinanceira transacao)
        {
            Tipo = transacao.Tipo;
            Valor = transacao.Valor;
            Data = transacao.Data;
            Modelo = new ModeloVeiculoDTO(transacao.ModeloVeiculo);
            ModeloId = Modelo.Id;
            Funcionario = new FuncionarioSimplesDTO(transacao.Funcionario);
            FuncionarioId = transacao.FuncionarioId;
        }
    }

    public class TransacaoFinanceiraCompletaDTO
    {
        public string Tipo { get; set; }
        public double Valor { get; set; }
        public DateTime Data { get; set; }
        public string Modelo { get; set; }
        public string Veiculo { get; set; }
        public string Marca { get; set; }
        public string TipoVeiculo { get; set; }

        public TransacaoFinanceiraCompletaDTO() { }

        public TransacaoFinanceiraCompletaDTO(TransacaoFinanceira transacao)
        {
            Tipo = transacao.Tipo;
            Valor = transacao.Valor;
            Data = transacao.Data;
            Modelo = transacao.ModeloVeiculo.Nome;
            Veiculo = transacao.ModeloVeiculo.Veiculo.Nome;
            Marca = transacao.ModeloVeiculo.Veiculo.MarcaVeiculo.Nome;
            TipoVeiculo = transacao.ModeloVeiculo.Veiculo.MarcaVeiculo.TipoVeiculo.Nome;
        }
    }
    #endregion

    #region Interface 
    public interface ITransacaoFinanceiraService
    {
        public Task CriarNovaTransacaoFinanceira(TransacaoFinanceiraDTO transacaoFinanceira);
    }
    #endregion
}
