﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ploomes.Domain
{
    public class TipoVeiculo
    {

        public int Id { get; private set; }
        public string Nome { get; private set; }
        public string NomeFipe { get; private set; }
        public DateTime? DataAtualizacao { get; private set; }
        // 1 (TipoVeiculo) - to - many (MarcasVeiculos)

        private HashSet<MarcaVeiculo> _marcasVeiculos;
        public IEnumerable<MarcaVeiculo> MarcasVeiculos => _marcasVeiculos?.ToList();

        private TipoVeiculo()
        {
            _marcasVeiculos = new HashSet<MarcaVeiculo>();
        }
        
        public void AdicionarMarcaVeiculo(MarcaVeiculoApiDTO marcaVeiculo)
        {
            var MarcaVeiculo = new MarcaVeiculo(marcaVeiculo, this);
            _marcasVeiculos.Add(MarcaVeiculo);
        }

        public void AtualizarDataAtualizacao()
        {
            DataAtualizacao = DateTime.Now;
        }


    }

    #region DTOs
    public class TipoVeiculoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
    #endregion

    #region Interface
    public interface ITipoVeiculoService
    {
        public Task AtualizarMarcasVeiculosAsync(TipoVeiculo tipoVeiculo);
        public Task<List<TipoVeiculoDTO>> GetTodosTiposVeiculos();
    }
    #endregion
}
