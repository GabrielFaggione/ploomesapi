﻿using Ploomes.Domain;
using Ploomes.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ploomes.API.Services
{
    public class TransacaoFinanceiraService : ITransacaoFinanceiraService
    {
        public TransacaoFinanceiraService(
           PloomesContext context
           )
        {
            Context = context;
        }

        private readonly PloomesContext Context;

        public async Task CriarNovaTransacaoFinanceira(TransacaoFinanceiraDTO transacaoFinanceira)
        {
            using (var tran = Context.Database.BeginTransaction())
            {
                var funcionario = Context.Funcionarios
                    .FirstOrDefault(f => f.Id == transacaoFinanceira.FuncionarioId);
                if (funcionario.Ativo)
                {
                    var transacao = new TransacaoFinanceira(transacaoFinanceira);
                    Context.Add(transacao);
                    await Context.SaveChangesAsync();
                    await tran.CommitAsync();
                }
                else
                {
                    throw new Exception("O Funcionário informado está desativado");
                }
            }
        }
    }
}
