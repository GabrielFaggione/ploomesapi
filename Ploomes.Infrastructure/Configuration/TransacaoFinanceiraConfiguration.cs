﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ploomes.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ploomes.Infrastructure.Configuration
{
    class TransacaoFinanceiraConfiguration : IEntityTypeConfiguration<TransacaoFinanceira>
    {
        public void Configure(EntityTypeBuilder<TransacaoFinanceira> builder)
        {
            builder.HasKey(c => c.Id);

            builder
                .HasOne(s => s.ModeloVeiculo)
                .WithMany(u => u.TransacoesFinanceiras)
                .HasForeignKey(s => s.ModeloVeiculoId);

            builder
                .HasOne(s => s.Funcionario)
                .WithMany(u => u.TransacoesFinanceiras)
                .HasForeignKey(s => s.FuncionarioId);

        }
    }
}
