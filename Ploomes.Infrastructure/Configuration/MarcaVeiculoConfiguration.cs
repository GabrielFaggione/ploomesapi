﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ploomes.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ploomes.Infrastructure.Configuration
{
    class MarcaVeiculoConfiguration : IEntityTypeConfiguration<MarcaVeiculo>
    {
        public void Configure(EntityTypeBuilder<MarcaVeiculo> builder)
        {
            builder.HasKey(c => c.Id);

            builder
                .HasOne(s => s.TipoVeiculo)
                .WithMany(u => u.MarcasVeiculos)
                .HasForeignKey(s => s.TipoVeiculoId);

            var element = builder.Metadata.FindNavigation(nameof(MarcaVeiculo.Veiculos));
            element.SetField("_veiculos");
            element.SetPropertyAccessMode(PropertyAccessMode.Field);

        }
    }
}
