﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Ploomes.Domain;
using Ploomes.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Ploomes.API.Services
{
    public class MarcaVeiculoService : IMarcaVeiculoService
    {

        public MarcaVeiculoService(
            PloomesContext context
            )
        {
            Context = context;
        }

        private readonly PloomesContext Context;

        public async Task AtualizarVeiculosAsync(MarcaVeiculo marcaVeiculo)
        {
            using (var tran = await Context.Database.BeginTransactionAsync())
            {
                if (marcaVeiculo.Veiculos.Count() == 0)
                {
                    using (var cliente = new HttpClient())
                    {
                        var resposta = await cliente.GetAsync(string.Format("http://fipeapi.appspot.com/api/1/{0}/veiculos/{1}.json", marcaVeiculo.TipoVeiculo.NomeFipe, marcaVeiculo.IdFipe));
                        resposta.EnsureSuccessStatusCode();
                        var respotaBody = await resposta.Content.ReadAsStringAsync();
                        var respostaDeserializada = JsonConvert.DeserializeObject<List<VeiculoApiDTO>>(respotaBody);

                        foreach (var veiculoDTO in respostaDeserializada)
                        {
                            if (!marcaVeiculo.Veiculos.Any(a => a.IdFipe == veiculoDTO.id))
                            {
                                marcaVeiculo.AdicionarVeiculo(veiculoDTO);
                            }
                        }
                    }
                    await Context.SaveChangesAsync();
                    await tran.CommitAsync();
                }
            }
        }

        public async Task<List<MarcaVeiculoSimplesDTO>> GetMarcasVeiculosPorTipoVeiculoId(int id)
        {
            var tipoVeiculo = await Context.TiposVeiculos
                .Include(i => i.MarcasVeiculos)
                    .ThenInclude(i => i.Veiculos)
                .FirstOrDefaultAsync(f => f.Id == id);
            var marcasVeiculos = tipoVeiculo.MarcasVeiculos;
            foreach (var marcaVeiculo in marcasVeiculos) await AtualizarVeiculosAsync(marcaVeiculo);
            var marcasVeiculosDTO = marcasVeiculos.Select(s => new MarcaVeiculoSimplesDTO(s)).ToList();
            return marcasVeiculosDTO;
        }

    }
}
